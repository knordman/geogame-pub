package com.example.geogame;




import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;





public class SpotSet extends Activity implements OnClickListener {
	
	private Button tp;
	private int lat, lon;
	 private EditText name;
	 private EditText text;
	 private EditText radius;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_spotset);
 
        
        Bundle extras = getIntent().getExtras();
        if(extras !=null){
        	
        		lat=extras.getInt("LAT");
        		
        		lon=extras.getInt("LON");
       		
        	}
        
        this.name = (EditText) findViewById(R.id.name);
        this.text = (EditText) findViewById(R.id.text);
        this.radius = (EditText) findViewById(R.id.radius);
        
        radius.setText("250");
        
        View setButton = findViewById(R.id.setButton);
        setButton.setOnClickListener(this);
        View cancelButton = findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);
        
	}
	
	
	@Override
	public void onClick(View v) {
		Intent i;
		Bundle extras;
	  	switch (v.getId()){
    	case R.id.setButton:
    		
    		i =new Intent(this, GGMapActivity.class);
    		extras = new Bundle();
    		extras.putInt("LAT", lat);
    		extras.putInt("LON",lon);
    		extras.putString("NAME", this.name.getText().toString());
    		
    		
    		
    		extras.putString("TEXT", this.text.getText().toString());
    		
    		int rad = Integer.parseInt(radius.getText().toString());
    		if(rad<100)
    			rad = 100;
    		else if(rad>1000)
    			rad = 1000;
    		
    		
    		extras.putInt("RADIUS", rad );
    		

    		i.putExtras(extras);
    		
    		setResult(RESULT_OK,i);
            //close this Activity...
            finish();
    		
    		break;
    	
    	case R.id.cancelButton:
    		
    		onBackPressed();
    		
    		break;
    	}
		
	}
	
	@Override
	public void onBackPressed() {
		Intent i =new Intent(this, GGMapActivity.class);

		// do something on back.
		setResult(RESULT_CANCELED, i);
		finish();
	    return;
	}
	

}
