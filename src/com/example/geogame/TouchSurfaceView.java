package com.example.geogame;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;

import com.example.geogame.logging.Logger;
import com.example.geogame.logging.LoggerEvent;
import com.example.geogame.tools.GeoPlayer;

public class TouchSurfaceView extends GLSurfaceView {
	//******************** 
	private CubeRenderer cr;
	//private TexampleRenderer cr;
	GeoPlayer player;
	private Context mContext;
	
	private float maxAngleDifference = 20;  //The cone will be maxAngleDifference*2
	private float angleOffset = 2.0f; // How much wider is the cone before the text
			
			
	//degrees wide.
	
    //********************
	public TouchSurfaceView(Context context, CubeRenderer cR, GeoPlayer player) { 
   // public TouchSurfaceView(Context context, TexampleRenderer cR, GeoPlayer player) { 
        super(context);
        mContext=context;
        this.cr = cR;
        this.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        this.setRenderer(cR);
        this.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);  
        this.getHolder().setFormat(PixelFormat.TRANSPARENT);

        //Player related stuff
        this.player = player;
        timeSinceLastToggle = 0;
        }  
    

/* We don't
/* We don't need the first one of these at all and we have to see whether
 * to implement the second one later and how.
 * 
 * 
 * 
    public boolean onTrackballEvent(MotionEvent e) {     
        cr.mAngleX += e.getX() * TRACKBALL_SCALE_FACTOR;      
        cr.mAngleY += e.getY() * TRACKBALL_SCALE_FACTOR;    
        requestRender();   
        return true;    }   
*/
    
    /*
     * Vector's magnitude
     */
    private float magnitude(float[] vector) {
    	int length = vector.length;
    	float result = 0;
    	for(int i = 0; i < length; i++) {
    		result += vector[i]*vector[i];
    	}
    	
    	return (float)Math.sqrt((double)result);
    }
    
    /*
     * Let A and B be vectors
     * The angle between them is:
     *            A�B          (dot product)
     * arccos( ------------ )
     *         ||A|| * ||B||
     *         
     * This will return a number 0 <= x <= 180 and will return the
     * "smaller" angle.
     */
    private float angleBetween(float[] vector1, float[] vector2) {
    	int length = vector1.length;
    	float result = 0;
    	
    	for(int i = 0; i < length; i++) {
    		result += vector1[i] * vector2[i];
    	}
    	result = result/(magnitude(vector1)*magnitude(vector2));
    	result = (float)(Math.acos((double) result));
    	return result * (180/(float)Math.PI);
    }
    
    /*
     * Calculates the distance from (x, y) to (0, 0)
     */
    private float distanceToCenter(float x, float y) {
    	float result = 0;
    	result = x*x + y*y;
    	
    	return (float)Math.sqrt((double)result);
    }
    
    /*
     * Returns the difference between two angles. These angles
     * are clockwise angles and are calculated by comparing two vectors
     * where the other vector is [0i, 1j].
     * 
     * The other angle is the sensor manager's azimuth angle.
     * 
     * Using this for anything else is ill-advised.
     */
    
    private boolean compareTwoAnglesBoolean(float angle1, float angle2) {
    //private float compareTwoAngles(float angle1, float angle2) {
    	//if(angle1 > 180) angle1 = angle1 - 2*(angle1 - 180);
    	//if(angle2 > 180) angle2 = angle2 - 2*(angle2 - 180);
    	
    	float loLim = angle2 - (maxAngleDifference); // 
    	boolean over= false;
    	boolean under= false;
    	
    	
    	if(loLim < 0){
    		loLim = 360.0f +loLim;
    		under=true;
    	}
    	
    	
    	float hiLim = angle2 + (maxAngleDifference*angleOffset);
    	
    	if(hiLim > 360.0f){
    		hiLim = hiLim - 360.0f;
    		over=true;
    	}
    	//return Math.abs(angle1 - angle2);
    	
    	if(!under && !over){
    	if(angle1 <= hiLim && angle1 >= loLim )
    		return true;
    	}
    	else {
    		if((angle1 >= loLim && angle1 <= 360.0f) || (angle1 >= 0.0f && angle1 <= hiLim))
    				return true;
    	}
   
    	return false;
    }
    
    private float compareTwoAngles(float angle1, float angle2) {
    	if(angle1 > 180) angle1 = angle1 - 2*(angle1 - 180);
    	if(angle2 > 180) angle2 = angle2 - 2*(angle2 - 180);
    		return Math.abs(angle1 - angle2);
    }
    /*
     * Detects if clicked on a location in camera view.
     * 
     * The idea is to make the device the center of the universe and then
     * fire a cone shaped laser from the back of the device. If this laser
     * hits a location, something happens.
     * 
     * In practice this is done using vectors and angles.
     * 
     * Some limitations:
     * The earth is assumed to be flat. This means the the third dimension
     * is lost in translation (read: ignored on purpose).
     * 
     * Latitude and longitude are used as coordinates on a 2D plane
     * and don't automatically wrap around the earth; this should be
     * considered when moving across the poles or when moving
     * on the opposite side of the planet (Australia or something).
     * 
     * If the list grows really big, one might want to replace the boolean list
     * with a dynamic list of locations or just come up with a better solution
     * altogether.
     */
    private long timeSinceLastToggle;
    @Override
    public boolean onTouchEvent(MotionEvent e) {
    	long currentTime = System.currentTimeMillis();
    	if(currentTime - timeSinceLastToggle < 500) {
    		return false;
    	}
    	timeSinceLastToggle = currentTime;

    	boolean[] list = new boolean[GGMapActivity.reminders.size()];
    	//float myDirection = cr.mRotZ;  //This is where we are pointing to
    	float myDirection = cr.fix_mRotZ;  //This is where we are pointing to
    	
    	
    	//Filter locations that are out of reach
    	for(int i = 0; i < list.length; i++) {
    		if(GGMapActivity.reminders.get(i).inRadius) {
    			list[i] = true;
    		} else {
    			list[i] = false;
    		}
    	}
    	
    	//Filter locations based on where you are looking
    	//lat = y, long = x
    	//float[] tempVector = new float[2];
    	//float[] unitVector = {0, 1};
    	for(int i = 0; i < list.length; i++) {
    		if(list[i]) {
    			//tempVector[0] = GGMapActivity.reminders.get(i).getLongitude() - (float)GGMapActivity.currentLongitude;
    			//tempVector[1] = GGMapActivity.reminders.get(i).getLatitude() - (float)GGMapActivity.currentLatitude;
    			
    			//float angle = angleBetween(tempVector, unitVector);  //Will return 0 <= x <= 180
    			float bear = GGMapActivity.reminders.get(i).getBearing();
    			if(bear < 0)
    				bear = 360.0f + bear;
    			
    			
    			/*if(tempVector[0] < 0) {  //Is the vector pointing towards west?
    				angle = 360 - angle;
    				
    			}*/
    			//Log.i("Success", "Angle to: " + GGMapActivity.reminders.get(i).getName()+ " is "+ angle +", bear: "+ bear + ", myDir: "+ myDirection );
    			/*if(compareTwoAngles(bear, myDirection) > maxAngleDifference) {
    				list[i] = false;
    			}*/
    			
    			list[i] = compareTwoAnglesBoolean(bear, myDirection);
    		}
    	}
    	
    	//Finally just get the closest one, if there's any left
    	SpotData closest = null;
    	float distance = Float.MAX_VALUE;
    	for(int i = 0; i < list.length; i++) {
    		if(list[i]) {
    			SpotData temp = GGMapActivity.reminders.get(i);
    			float newDist = distanceToCenter(temp.getLongitude() - GGMapActivity.currentLongitude, temp.getLatitude() - GGMapActivity.currentLatitude);
    			if(newDist < distance) {
    				closest = temp;
    				distance = newDist;
    			}
    		}
    	}

    	//We found the closest visible location
    	if(closest != null) {
    		FlatActivity act = (FlatActivity)mContext;
    		Log.d("Success", "Closest: " + closest.getName() );
    		
    		// If audio file is existing stop possible synth sound and toggle audio
    	
    			String resName = closest.getResourceName();
    			
    			
	        	   if(act.isSpeaking())
	        		   act.stopSpeaking();	    			
 
    			if(resName!=null && !resName.trim().equals(""))	{
    				player.toggle(closest.getResourceName());
    			}
    		
    			// otherwise play description or text with speech synth
    			else{
    				if(player!=null && player.isPlaying()){
    					player.destroy();
    				}
    				
    				if(closest.getDescription()!=null && !closest.getDescription().trim().equals("")){
    					act.speakWords(closest.getDescription());
    					
    				}	
    				else if(closest.getText()!=null)
    					act.speakWords(closest.getText());
    			}
    		   
    			Logger.log(new TouchSurfaceViewLoggerEvent(closest, LoggerEvent.TAP_EVENT));
    		}
    		else {
        	Logger.log(new TouchSurfaceViewLoggerEvent(null, LoggerEvent.TAP_EVENT));
    	}
    	
        return true;  
    }
 
//    private final float TOUCH_SCALE_FACTOR = 180.0f / 320;  
//    private final float TRACKBALL_SCALE_FACTOR = 36.0f;   
//    private float mPreviousX;   
//    private float mPreviousY;

    
  
    
    

}

class TouchSurfaceViewLoggerEvent extends LoggerEvent {
	private SpotData location;
	/**
	 * 
	 * @param eventType  Should be one of those found in LoggerEvent class.
	 */
	public TouchSurfaceViewLoggerEvent(SpotData location, int eventType) {
		super(eventType);
		this.location = location;
	}
	
	@Override
	public String makeReport() {
		String report = formatDate();
		report += " ";
		report += EVENT_TYPES[getEventType()];
		if(location == null) {
			report += ": User tapped the camera preview screen ";
		} else {
			report += ": Use tapped a location on the camera preview screen -- ";
			report += "Location was: " + location.getName();
		}
		
		//Doesn't work like this
//		Configuration config = view.getResources().getConfiguration();
//		if(config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//			report += "(vertical)";
//		} else if(config.orientation == Configuration.ORIENTATION_PORTRAIT) {
//			report += "(horizontal)";
//		} else {
//			report += "(unkown orientation)";
//		}
		
		return report;
	}
	
	
	
	
	
	
	
	
}