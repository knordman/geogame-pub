package com.example.geogame;

import android.content.Context;
import android.content.res.Configuration;
import android.view.View;
import android.widget.Toast;

import com.example.geogame.logging.Logger;
import com.example.geogame.logging.LoggerEvent;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;

/**
 * Overlay for displaying user's location on map.
 * @author Team Radia, Univ. Helsinki, Dept. CS
 *
 */
public class UserLocationOverlay extends MyLocationOverlay {
	Context mContext;

	/**
	 * 
	 * @param context
	 * @param mapView
	 */
	public UserLocationOverlay(Context context, MapView mapView) {
		super(context, mapView);
		mContext = context;
		enableMyLocation();
	}

	// What to do when tapped on any location on map.
	public boolean onTap(GeoPoint p, MapView map) {
		super.onTap(p, map);
		
    	Logger.log(new UserLocationOverlayLoggerEvent(LoggerEvent.TAP_EVENT, p));
		return false;
	}

	
	long lastLocationHintTime = 0;
	// What to do when the location tapped was near the user's own location.
	public boolean dispatchTap() {
		
		long currentTime = System.currentTimeMillis();
		if (currentTime - lastLocationHintTime > 5000) {
			
			Toast.makeText(mContext, "Your location ", Toast.LENGTH_SHORT).show();
			
			//Toast.makeText(mContext, "Your location "+ super.getLastFix().getLatitude()+", "+ super.getLastFix().getLongitude()+"\nMng. loacation: " +
			//GGMapActivity.currentLatitude+", "+ GGMapActivity.currentLongitude, Toast.LENGTH_LONG).show();
			
			lastLocationHintTime = currentTime;
		}
		return false;
	}
	

}

class UserLocationOverlayLoggerEvent extends LoggerEvent {
	private GeoPoint p;
	
	/**
	 * 
	 * @param eventType  Should be one of those found in LoggerEvent class.
	 * @param p
	 */
	public UserLocationOverlayLoggerEvent(int eventType, GeoPoint p) {
		super(eventType);
		this.p = p;
	}
	
	@Override
	public String makeReport() {
		String report = formatDate();
		report += " ";
		report += EVENT_TYPES[getEventType()];
		report += ": User tapped a location on the map - ";
		report += " LAT: " + p.getLatitudeE6() + " LON: " + p.getLongitudeE6();
		return report;
	}
	
}