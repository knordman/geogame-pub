package com.example.geogame;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.opengl.GLUtils;
import android.util.Log;

class Cube{
	
	private float y = 1.0f;
	private float x = 1.0f;//0.5f;
	private float z = 1.0f;//0.01f;
	private FloatBuffer vertexBuffer;
	private FloatBuffer textureBuffer;
	private ByteBuffer indexBuffer;
	
	private String text = "";
	
	private int[] textures = new int[1]; // texture pointer
	
    private float vertices[] = {
    					//Vertices according to faces
			    		-x, -y, z, 
			    		x, -y, z,  
			    		-x, y, z,  
			    		x, y, z,   
			    		
			    		x, -y, z,
			    		x, -y, -z,    		
			    		x, y, z,
			    		x, y, -z,
			    		
			    		x, -y, -z,
			    		-x, -y, -z,    		
			    		x, y, -z,
			    		-x, y, -z,
			    		
			    		-x, -y, -z,
			    		-x, -y, z,    		
			    		-x, y, -z,
			    		-x, y, z,
			    		
			    		-x, -y, -z,
			    		x, -y, -z,    		
			    		-x, -y, z,
			    		x, -y, z,
			    		
			    		-x, y, z,
			    		x, y, z,    		
			    		-x, y, -z,
			    		x, y, -z
	};
    
    private float texture[] = {    		
			    		0.0f, 0.0f,
			    		0.0f, 1.0f,
			    		1.0f, 0.0f,
			    		1.0f, 1.0f, 
			    		
			    		0.0f, 0.0f,
			    		0.0f, 1.0f,
			    		1.0f, 0.0f,
			    		1.0f, 1.0f,
			    		
			    		0.0f, 0.0f,
			    		0.0f, 1.0f,
			    		1.0f, 0.0f,
			    		1.0f, 1.0f,
			    		
			    		0.0f, 0.0f,
			    		0.0f, 1.0f,
			    		1.0f, 0.0f,
			    		1.0f, 1.0f,
			    		
			    		0.0f, 0.0f,
			    		0.0f, 1.0f,
			    		1.0f, 0.0f,
			    		1.0f, 1.0f,
			    		
			    		0.0f, 0.0f,
			    		0.0f, 1.0f,
			    		1.0f, 0.0f,
			    		1.0f, 1.0f
    };
        
    private byte indices[] = {
			    		0,1,3, 0,3,2, 			//Face front
			    		4,5,7, 4,7,6, 			//Face right
			    		8,9,11, 8,11,10, 		//... 
			    		12,13,15, 12,15,14, 	
			    		16,17,19, 16,19,18, 	
			    		20,21,23, 20,23,22, 	
    };

	public Cube() {
		//
		ByteBuffer byteBuf = ByteBuffer.allocateDirect(vertices.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		vertexBuffer = byteBuf.asFloatBuffer();
		vertexBuffer.put(vertices);
		vertexBuffer.position(0);

		byteBuf = ByteBuffer.allocateDirect(texture.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		textureBuffer = byteBuf.asFloatBuffer();
		textureBuffer.put(texture);
		textureBuffer.position(0);

		indexBuffer = ByteBuffer.allocateDirect(indices.length);
		indexBuffer.put(indices);
		indexBuffer.position(0);
		// Log.d("cube", "done const");
	}
	
	public Cube(String txt) {
		setText(txt);
		
		ByteBuffer byteBuf = ByteBuffer.allocateDirect(vertices.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		vertexBuffer = byteBuf.asFloatBuffer();
		vertexBuffer.put(vertices);
		vertexBuffer.position(0);

		byteBuf = ByteBuffer.allocateDirect(texture.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		textureBuffer = byteBuf.asFloatBuffer();
		textureBuffer.put(texture);
		textureBuffer.position(0);

		indexBuffer = ByteBuffer.allocateDirect(indices.length);
		indexBuffer.put(indices);
		indexBuffer.position(0);
		 //Log.d("cube", "done text const");
		
	}
	

	public void draw(GL10 gl) {
		//Bind our only previously generated texture in this case
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
		
		//Point to our buffers
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

		//Set the face rotation
		gl.glFrontFace(GL10.GL_CCW);
		
		//Enable the vertex and texture state
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
		
		//Draw the vertices as triangles, based on the Index Buffer information
		gl.glDrawElements(GL10.GL_TRIANGLES, indices.length, GL10.GL_UNSIGNED_BYTE, indexBuffer);
		
		//Disable the client state before leaving
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		//Log.d("cube", "done draw");
	}
	
	public void setText(String txt){
		if(txt!=null)
			this.text=txt;
		else
			this.text="";
	}
	
	public void loadGLTexture(GL10 gl, Context context) {
		
		// get a background image from resources
				// note the image format must match the bitmap format
				/*Drawable background = context.getResources().getDrawable(R.drawable.square);
				background.setBounds(0, 0, 256, 256);
				background.draw(canvas); // draw the background to our bitmap*/
				
				// Draw the text
				Paint textPaint = new Paint();
				textPaint.setTextSize(24);
				textPaint.setFakeBoldText(true);
				textPaint.setAntiAlias(true);
				textPaint.setARGB(255, 255, 255, 255);
					
				// Create an empty, mutable bitmap
				Bitmap bitmap = Bitmap.createBitmap(256, 256, Bitmap.Config.ARGB_4444);;
				bitmap.eraseColor(Color.TRANSPARENT); // get a canvas to paint over the bitmap
				if(textPaint.measureText(text) < 256){
					
					Canvas canvas = new Canvas(bitmap);
					canvas.drawText(text, 0, 125, textPaint);
					
				}
				else{			
					String[] toRender = getStrings2Render(text, textPaint);
					Canvas canvas = new Canvas(bitmap);
					
					for(int i = 0; i<toRender.length && toRender[i] != null; i++){
						canvas.drawText(toRender[i], 0, 30+(i*30), textPaint);
					}
				}				
				
				//Generate one texture pointer...
				gl.glGenTextures(1, textures, 0);
				//...and bind it to our array
				gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

				//Create Nearest Filtered Texture
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

				//Different possible texture parameters, e.g. GL10.GL_CLAMP_TO_EDGE
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);

				//Use the Android GLUtils to specify a two-dimensional texture image from our bitmap
				GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

				//Clean up
				bitmap.recycle();
				
				/*
				//Get the texture from the Android resource directory
				InputStream is = context.getResources().openRawResource(R.drawable.glass);
				Bitmap bitmap = null;
				try {
					//BitmapFactory is an Android graphics utility for images
					bitmap = BitmapFactory.decodeStream(is);

				} finally {
					//Always clear and close
					try {
						is.close();
						is = null;
					} catch (IOException e) {
					}
				}
				
				//Generate one texture pointer...
				gl.glGenTextures(1, textures, 0);
				//...and bind it to our array
				gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
				
				//Create Nearest Filtered Texture
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

				//Different possible texture parameters, e.g. GL10.GL_CLAMP_TO_EDGE
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);
				
				//Use the Android GLUtils to specify a two-dimensional texture image from our bitmap
				GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
				
				//Clean up
				bitmap.recycle();*/
				//Log.d("cube", "done load");
	}
	
private String[] getStrings2Render(String s, Paint textPaint) {
		
		String[] sArray = s.split(" ");
		String[] retVal= new String[10];
		
		String toArray = "";
		String temp="";
		int index=0;
		for(int i=0; i<sArray.length; i++){
			temp = temp + " " + sArray[i];
			int measure = (int) textPaint.measureText(temp);
			if(measure < 256){
				toArray = temp;
			}
			else{
				retVal[index] = toArray;
				temp="";
				toArray="";
				i--;
				index++;
			}
		}
		if(!toArray.equals(""))
			retVal[index] = toArray;
		
		return retVal;
	}
}