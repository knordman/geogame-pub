package com.example.geogame;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class GGOverlayItem extends OverlayItem {
	
	private String resourceName;
	private int index;
	
	
	public GGOverlayItem(GeoPoint point, String title, String snippet, String resName, int idx){
		super(point, title, snippet);
		resourceName = resName;
		index = idx;
		
	}
	
	public String getResourceName(){
		return resourceName;
	}
	
	public int getIndex(){
		return index;
	}
	
	
	

}
