package com.example.geogame;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.location.Location;
import java.io.Serializable;

import com.google.android.maps.GeoPoint;

public class SpotData implements Serializable{
	/**
	 * Random generated serialVersionUID for object serialization
	 */
	private static final long serialVersionUID = -352382621614813877L;
	private String text, name, description;
	private List<String> list;
	private int latitude, longitude;
	private long remindAfter, remindBefore;
	private boolean addressSet = false;
	private int radius;
	private float distance;
	private float bearing;
	private String resourceName;  //This name should point to the resource in the 'raw' folder and in the R.java.
	                              //Filenames can only have these characters: [a-z0-9_.].
	public boolean inRadius=false;
	public boolean active= false;
	
	public long radiusVibraTime = 0;
	//public boolean play=false;
	public boolean vibraIn = false;
	
	
	public SpotData(){
		text = null;
		list = null;
		name = null;
		resourceName = null;
		latitude = -1;
		longitude = -1;
        remindBefore = 0;
        Date date = new Date(System.currentTimeMillis());
        remindAfter = date.getTime(); 
        radius=400;
        distance = -1.0f;
        description = null;
        
	};
	
	public SpotData(String text, String name, int lat, int lng, int rad){
		this();
		this.text = text;
		this.name = name;
		this.radius = rad;
		
		setLocation(lat, lng);
	};
	
	public SpotData(String text, String name, int lat, int lng, int rad, String res){
		this();
		this.text = text;
		this.name = name;
		this.radius = rad;
		if(res!=null)
			this.resourceName = res;
		
		setLocation(lat, lng);
	};
	

	public SpotData(String text, String name, int lat, int lng){
		this();
		this.text = text;
		this.name = name;
		setLocation(lat, lng);
	};
	
	
	public SpotData(String text, int lat, int lng, int rad){
		this(text, Integer.toString(lat)+","+Integer.toString(lng), lat, lng, rad);
	};
	
	public SpotData(String text, int lat, int lng, int rad, String res){
		this(text, Integer.toString(lat)+","+Integer.toString(lng), lat, lng, rad);
		if(res!=null)
			this.resourceName = res;
	};
	
	public SpotData(String text, int lat, int lng){
		this(text, Integer.toString(lat)+","+Integer.toString(lng), lat, lng);
	};
	
	public SpotData(String text, String name, double lat, double lng){
		this();
		this.text = text;
		this.name = name;
		setLocation(lat, lng);
	};
	
	public SpotData(String text, double lat, double lng){
		this(text, Double.toString(lat)+","+Double.toString(lng), lat, lng);
	};
		
	public String getText(){
		return text;
	}
	
	public float getDistance(){
		return distance;
	}
	
	public void setDistance(float dist){
		this.distance = dist;
	}
	
	public int getLatitude(){
		return latitude;
	}
	
	public int getLongitude(){
		return longitude;
	}

	public double getLatitudeDouble(){
		return latitude/1e6;
	}
	
	public double getLongitudeDouble(){
		return longitude/1e6;
	}

	public void setRadius(int rad){
		this.radius = rad;
	}

	public int getRadius(){
		return this.radius;
	}
	
	public void setBearing(float bear){
		this.bearing = bear;
	}

	public float getBearing(){
		return this.bearing;
	}
	

	
	
	public void setText(String text){
		this.text = text;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}
	
	public String getDescription(){
		return this.description;
	}

	public void setDescription(String desc){
		this.description = desc;
	}
	
	public void setLocation(Location location){
        this.latitude = (int)(location.getLatitude()*1e6);
        this.longitude = (int)(location.getLongitude()*1e6);
	}

	public void setLocation(int lat, int lng){
		this.latitude = lat;
		this.longitude = lng;
	}

	public void setIsAddressFound(boolean isSet){
		this.addressSet=isSet;
		
	}
	
	public boolean getIsAddressFound(){
		return this.addressSet;
	}
	
	public void setLocation(double lat, double lng){
		this.setLocation((int)(lat*1e6), (int)(lng*1e6));
	}
	
	public Location getLocation(){
		GeoPoint newCurrent = new GeoPoint(latitude, longitude);
        Location current = new Location("reverseGeocoded");
        current.setLatitude(newCurrent.getLatitudeE6() / 1e6);
        current.setLongitude(newCurrent.getLongitudeE6() / 1e6);
        current.setAccuracy(3333);
        current.setBearing(333);
		return current;
	}
	
	public GeoPoint getGeoPoint() {
		return new GeoPoint(latitude, longitude);
	}
	
	public List<String> getList(){
		return list;
	}

	public void addListItem(String item){
		list.add(item);
	}
	
	public void removeListItem(String item){
	
	}

	public void setRemindAfter(Calendar time){
		this.remindAfter = calendarToLong(time);
	}

	public Calendar getRemindAfter(){
		return longToCalendar(remindAfter);
	}
	
	public void setRemindBefore(Calendar time){
		this.remindBefore = calendarToLong(time);
	}

	public Calendar getRemindBefore(){
   		return longToCalendar(remindBefore);
	}
	
	public boolean getIsBeforeSet(){
   		if(remindBefore <= 0)
   			return false;
   		else
   			return true;
	}
	
	public void setResourceName(String resourceName) {
		if(resourceName.equals("")) {
			return;
		}
		this.resourceName = resourceName;
	}

	public String getResourceName() {
		return this.resourceName;
	}

	private long calendarToLong(Calendar cal){
		Date date = cal.getTime(); 
		long time = date.getTime();
		return time;
	}
	private Calendar longToCalendar(long time){
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        cal.setTime(new Date(time));
		return cal;
	}
}
