package com.example.geogame;



import android.content.Context;
import android.util.AttributeSet;

/**
 * The map for the map activity. This class is responsible for the map itself that is used in the map activity.
 * At the moment, this does not alter the behavior of Google's standard MapView, but is added here for easy 
 * modifications in the future.
 * @author Team Radia, Univ. Helsinki, Dept. CS
 *
 */
public class GGMapView extends com.google.android.maps.MapView {

	/**
	 * The more manual constructor. This one can be used if the API key is to be provided through constructor call.
	 * @param context	The Context from which this was called.
	 * @param apiKey	The API key for accessing the Google Maps API
	 */
	public GGMapView(Context context, String apiKey) {
		super(context, apiKey);
		constructorCommon();
	}

	/**
	 * The default constructor. This is the default constructor run when providing the attributes through Android's resource container.
	 * @param context	The Context from which this was called.
	 * @param attrs		The attribute set, usually through Android's resources.
	 */
	public GGMapView(Context context, AttributeSet attrs) {
		super(context, attrs);
		constructorCommon();
	}

	
	/**
	 * Common functionality for all constructors.
	 */
	private void constructorCommon() {
		// Stuff common to all constructors can be run here.
	}

}