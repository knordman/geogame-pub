package com.example.geogame;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.MapActivity;

import com.example.geogame.SpotData;
import com.example.geogame.logging.Logger;
import com.example.geogame.logging.LoggerEvent;
import com.example.geogame.tools.GeoPlayer;


/**
 * The overlay for reminder markers on the map.
 * This class provides the overlay for reminder markers on the map, methods to add markers and
 * a way to identify which reminder was tapped on the map.
 * @author Team Radia, Univ. Helsinki, Dept. CS
 *
 */


public class MapMarkerOverlay extends ItemizedOverlay {
	private ArrayList<GGOverlayItem> mOverlays = new ArrayList<GGOverlayItem>();
	Context mContext;
	MapView mapView;
	GeoPlayer player;

	/**
	 * Creates an overlay that uses the icon defined as <code>defaultMarker</code> as the marker.
	 * @param defaultMarker	The <code>Drawable</code> to use as a reminder marker.
	 */
	public MapMarkerOverlay(Drawable defaultMarker, MapView mapView, GeoPlayer play) {
		super(boundCenter(defaultMarker));
		this.mapView = mapView;
		this.player=play;
		populate();
	}

	/**
	 * Creates an overlay that uses the icon defined as <code>defaultMarker</code> as the marker and <code>context</code> as the context.
	 * The context is currently used to determine where to draw dialogs.
	 * @param defaultMarker	The <code>Drawable</code> to use as a reminder marker.
	 * @param context	The <code>Context</code> to use as a target for dialogs.
	 */
	public MapMarkerOverlay(Drawable defaultMarker, Context context, MapView mapView, GeoPlayer play) {
		this(defaultMarker, mapView, play);
		mContext = context;
		populate();
	}

	/**
	 * Sets new context. Usable mainly if initiated with a constructor that didn't provide this.
	 * @param context	The <code>Context</code> to use as a target for dialogs.
	 */
	public void setContext(Context context) {
		mContext = context;
	}

	@Override
	protected OverlayItem createItem(int i) {
		return mOverlays.get(i);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}

	/**
	 * Add an item to the overlay on map.
	 * @param overlay	The <code>OverlayItem</code> to be added
	 */
	public void addOverlay(GGOverlayItem overlay) {
		mOverlays.add(overlay);
		populate();
	}

	/**
	 * Add a marker (an overlay item) to the map.
	 * @param lat	Latitude of the location in E6 decimal format
	 * @param lon	Latitude of the location in E6 decimal format
	 * @param title	Title when displaying the marker in a dialog
	 * @param message	Message when displaying the marker in a dialog
	 */
	public void addMarker(int lat, int lon, String title, String message) {
		GeoPoint location = new GeoPoint(lat, lon);
		addMarker(location, title, message, "", 0);

	}

	/**
	 * Add a marker (an overlay item) to the map.
	 * @param location	The <code>GeoPoint</code> representing the location
	 * @param title	Title when displaying the marker in a dialog
	 * @param message	Message when displaying the marker in a dialog
	 */
	public void addMarker(GeoPoint location, String title, String message, String resName, int idx) {
		GGOverlayItem item = new GGOverlayItem(location, title, message, resName, idx );
		addOverlay(item);
	}

	/**
	 * Add a marker (an overlay item) to the map. This version is for the internal 
	 * reminder representation of our application.
	 * @param reminder	The <code>SpotData</code> object to be added as a marker
	 */
	public void addMarker(SpotData reminder, int idx) {
		addMarker(reminder.getGeoPoint(), reminder.getName(), reminder.getText(), reminder.getResourceName(), idx);
	}

	/**
	 * Display an alert dialog on the map. The map has to be previously set as the context.
	 * @param title	Title of the message
	 * @param message	Content of the message
	 */
	private void alert(String title, String message, final String resName, final int idx) {
		final GGMapActivity act = (GGMapActivity)mContext;
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setCancelable(true);
		     
			// Set audio play button
			builder.setPositiveButton("Audio", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	   
		        	   
		        	   if(act.isSpeaking())
		        		   act.stopSpeaking();
		    			
		    			if(resName!=null && !resName.equals(""))	
		    				player.toggle(resName);
		    			else{
		    				
		    				if(player!=null && player.isPlaying()){
		    					player.destroy();
		    				}
		    				
		    				String description=GGMapActivity.reminders.get(idx).getDescription();
		    				if(description!=null && !description.equals(""))
		    					act.speakWords(description);
		    					
		    				else if(GGMapActivity.reminders.get(idx).getText()!=null)
		    					act.speakWords(GGMapActivity.reminders.get(idx).getText());
		    			}
		           }
		       });
		     
		     
		     
		  // Setting Netural audio stop button
		     
		     if(act.isSpeaking() || player.isPlaying()){
		    	 builder.setNeutralButton("Stop Audio", new DialogInterface.OnClickListener() {
		    		 public void onClick(DialogInterface dialog, int which) {
		    			 if(act.isSpeaking()){
		    				 act.stopSpeaking();
		    			 }
		    			 if(player.isPlaying()){
		    				 player.destroy();
		    			 }
		    		 }
		    	 });
		     }
		//AlertDialog alert = builder.create();
		//alert.show();
		//AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
		
		// Setting Negative back Button
        builder.setNegativeButton("Back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
           
            dialog.cancel();
            }
        });
        
		builder.setTitle(title);
		float dist = GGMapActivity.reminders.get(idx).getDistance();
		String dist_String ="";
		if(dist < 1000.0f)
			dist_String =  Math.round(dist)+" m\n";
		else
			dist_String =  String.format("%.2f", dist/1000)+" km\n";
		builder.setMessage("Distance: "+ dist_String+ message);
		builder.show();
	}
	
	@Override
	protected boolean onTap(int index) {
		GGOverlayItem item = mOverlays.get(index);
		alert(item.getTitle(), item.getSnippet(), item.getResourceName(), item.getIndex());
		
		Logger.log(new MapMarkerOverlayLoggerEvent(LoggerEvent.TAP_EVENT, item));
		
		return false;
	}
	
	
	
	float downX, downY, upX, upY, deltaX, deltaY, dist;
	long timeDelta;
	@Override
	public boolean onTouchEvent(MotionEvent event, MapView mapView) {
		if (event.getAction() == MotionEvent.ACTION_UP) {
			upX = event.getX();
			upY = event.getY();
			deltaX = Math.abs(downX-upX);
			deltaY = Math.abs(downY-upY);
			dist = FloatMath.sqrt((deltaX*deltaX)+(deltaY*deltaY));
			timeDelta = event.getEventTime() - event.getDownTime();
			
			if (dist < 20 && timeDelta > 800) {
				//DebugTeller.debugMsg(this, "Long press: "+ timeDelta +"ms  x: " + upX + "  y: " + upY);
				longPress(upX, upY);
				return true;
			}
			
		} else if (event.getAction() == MotionEvent.ACTION_DOWN) {
			downX = event.getX();
			downY = event.getY();
		}
		return super.onTouchEvent(event, mapView);
	}

	/**
	 * Called when a long press on the map has been detected.
	 * @param x	Screen X coordinate
	 * @param y	Scteen Y coordinate
	 */
	
	public void longPress(float x, float y) {
		// We know the map, we can deduce the map coordinates from the screen coordinates.	
		GeoPoint mapCoordinates = mapView.getProjection().fromPixels((int) x, (int) y);

		
	//	GGMapActivity.reminders.add(new SpotData("This is added test spot "+ GGMapActivity.newSpots, GGMapActivity.newSpots +". New Spot", mapCoordinates.getLatitudeE6(), mapCoordinates.getLongitudeE6(), 350, ""));
	//	GGMapActivity.newSpots++;
	//	((GGMapActivity)mContext).showOverlays();
		
		
		// We have coordinates for the long press, now here should be what we do with them.
		//Toast.makeText(mContext, "Selected: " + mapCoordinates.toString(), Toast.LENGTH_SHORT).show();

		MapActivity activity = ((MapActivity) mContext);
		Intent intent = new Intent(activity, SpotSet.class);
		//Bundle extras = intent.getExtras();
		
		Bundle extras = new Bundle();
		extras.putInt("LAT",mapCoordinates.getLatitudeE6());
		extras.putInt("LON", mapCoordinates.getLongitudeE6());
		
		
		intent.putExtras(extras);
		activity.startActivityForResult(intent, GGMapActivity.NEW_SPOT);

		
	}
	
}

class MapMarkerOverlayLoggerEvent extends LoggerEvent {
	private OverlayItem item;
	
	/**
	 * 
	 * @param eventType  Should be one of those found in LoggerEvent class.
	 * @param item
	 */
	public MapMarkerOverlayLoggerEvent(int eventType, OverlayItem item) {
		super(eventType);
		this.item = item;
	}
	
	@Override
	public String makeReport() {
		String report = formatDate();
		GeoPoint p = item.getPoint();
		report += " ";
		report += EVENT_TYPES[getEventType()];
		report += ": User tapped a marker on the map - ";
		report += item.getTitle() + " LAT: " + p.getLatitudeE6() + " LON: " + p.getLongitudeE6();
		return report;
	}
	
}