package com.example.geogame;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.geogame.logging.Logger;
import com.example.geogame.logging.LoggerEvent;
import com.example.geogame.tools.GeoPlayer;

public class FlatActivity extends Activity implements SensorEventListener, SurfaceHolder.Callback, OnInitListener {
	
    private Camera camera;
    private SurfaceView mSurfaceView;
    SurfaceHolder mSurfaceHolder;
    
    //private CameraPreview mPreview;

    private TouchSurfaceView mGLSurfaceView;
    //*********
    private CubeRenderer cr;
   
    private boolean previewRunning = false;
    private boolean change;
    
    GeoPlayer player;
    
    
	//TTS object
    	private TextToSpeech myTTS;
	//status check code
    	private int MY_DATA_CHECK_CODE = 0;

    ShutterCallback shutter = new ShutterCallback(){
    	
    	
        @Override
        public void onShutter() {
            // TODO Auto-generated method stub
            // No action to be perfomed on the Shutter callback.

        }

       };
       
    PictureCallback raw = new PictureCallback(){
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            // TODO Auto-generated method stub
            // No action taken on the raw data. Only action taken on jpeg data.
      }

       };

    PictureCallback jpeg = new PictureCallback(){

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            // TODO Auto-generated method stub

            FileOutputStream outStream = null;
            try{
                outStream = new FileOutputStream("/sdcard/test.jpg");
                outStream.write(data);
                outStream.close();
            }catch(FileNotFoundException e){
                Log.d("Camera", e.getMessage());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                Log.d("Camera", e.getMessage());
            }
        }

       };

	private SensorManager mSensorManager;
	private Sensor mOrientation;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        change=false;
        
      // mPreview = new CameraPreview(this, camera);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //****************
        cr = new CubeRenderer(true, this);
      
        
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        		WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.activity_flat);
        
       /* if(camera==null){
        	camera = Camera.open();
        	Log.d("FlatAct", "Opening cam from onCreate");
        }*/
        
        
        GGApp appData = ((GGApp)getApplicationContext());
	    player = appData.player;



        //player = new GeoPlayer(this);
         mGLSurfaceView = new TouchSurfaceView(this, cr, player); 
         addContentView(mGLSurfaceView, new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
     
         mSurfaceView = new SurfaceView(this);
         addContentView(mSurfaceView, new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
         mSurfaceHolder = mSurfaceView.getHolder();
         mSurfaceHolder.addCallback(this);
         mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
         mSurfaceHolder.setFormat(PixelFormat.TRANSLUCENT); 
         
         //Log.i("Here: ", "are we flatting. ");
		
         //Sensor stuff
         mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		
		
		//check for TTS data
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);

        Log.i("FlatAct CALLER", "onCreate");
        
	}

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
    	Log.i("FlatAct CALLER", "surfaceCreated");
    	//if(camera==null){
    	try {
            // Gets to here OK
			 camera = Camera.open();
			 camera.setPreviewDisplay(holder);
			 Log.d("FlatAct", "Opening cam from surfaceCreated");
	        
        } catch (Exception e) {
        	
        	if(camera != null)
        		camera.release();
            camera = null;
            e.printStackTrace();
            
            Log.i("NO CAMERA", "going down");
            //  throws runtime exception :"Failed to connect to camera service"
            finish();
           
        }
    	//camera = Camera.open();
    	//Log.d("FlatAct", "Opening cam from surfaceCreated");
    	//}
    }
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.i("FlatAct CALLER", "onResume");
		
		/*if(camera==null){
			
			 try {
                 // Gets to here OK
				 camera = Camera.open();
				 Log.d("FlatAct", "Opening cam from onResume");
		        
             } catch (Exception e) {
                 e.printStackTrace();
                 //  throws runtime exception :"Failed to connect to camera service"
                 finish();
                 
             }
    	
		}*/
		
		mGLSurfaceView.requestRender();	
		mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_NORMAL);
		
		//Create a new instance of a player
		if(player == null) {
			player = new GeoPlayer(this);
			
		
			
		}	
		
		
		Logger.log(new FlatActivityLoggerEvent(LoggerEvent.RESUMED_EVENT));
	}
	
	@Override
	protected void onPause() {
		
		Log.i("FlatAct CALLER", "onPause");
		 
		            
		
		
		//Release player
		mSensorManager.unregisterListener(this);
		
		
		Logger.log(new FlatActivityLoggerEvent(LoggerEvent.PAUSED_EVENT));
		super.onPause();
	}
	
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub

    	Log.i("FlatAct CALLER", "surfaceDestroyed");
    	
    	 if(camera != null)
		    {
		        camera.setPreviewCallback(null);
		        camera.stopPreview();
		        camera.setPreviewCallback(null);
		        previewRunning = false;
		        camera.release();
		        camera = null;
		        Log.d("FlatAct", "Camera down from onPause");
		    }
    	
    	
		/*if(player!=null){
			player.destroy();
			player = null;
		}*/
  
    }
	
	@Override
	protected void onStop() {
		
		Log.i("FlatAct CALLER", "onStop");
		mSensorManager.unregisterListener(this);
		super.onStop();
	}
	
	
	@Override
	protected void onDestroy() {
	    
		Log.i("FlatAct CALLER", "onDestroy");
		//Close the Text to Speech Library
		
	   if(myTTS != null) {

	    	//myTTS.stop();
	    	myTTS.shutdown();
	        Log.d("myTTS", "TTS Destroyed");
	    }
	    super.onDestroy();
	}
	
	public void goBack(View view) {
		finish();
	}
	
    private void takePicture() {
        // TODO Auto-generated method stub
        camera.takePicture(shutter, raw, jpeg);
    }
	
    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    //	camera.setDisplayOrientation(90);
        // TODO Auto-generated method stub
    	
    	if(previewRunning )
        {
            camera.stopPreview();
        }
    	
    	Log.i("FlatAct CALLER", "surfaceChanged");
        Camera.Parameters p = camera.getParameters();
        camera.setParameters(p);
       // p.setPreviewSize(arg2, arg3);
        
        /*if(android.os.Build.VERSION.SDK_INT >= 8)
        {   // If API >= 8 -> rotate display...
            camera.setDisplayOrientation(90);
        }*/
        
        
        try {
        	camera.setPreviewDisplay(arg0);
        } catch (IOException e) {
        e.printStackTrace();
        }
        camera.startPreview();
        previewRunning = true;
    }
	
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		float azimuth_angle = event.values[0]; //0 to 359
		float pitch_angle = event.values[1]; //180 to -180
		float roll_angle = event.values[2]; //90 to -90
		
		float fix_azimuth=azimuth_angle;


				if (pitch_angle < -45 && pitch_angle > -135) {
					//"Top side of the phone is Up!");
					cr.orient=0;
         	
					
                } else if (pitch_angle > 45 && pitch_angle < 135) {

            	        //"Bottom side of the phone is Up!");
                	fix_azimuth=azimuth_angle -180.0f;
                	if(fix_azimuth < 0){
                		fix_azimuth = 360.0f +fix_azimuth;
                	}
                		
                	cr.orient=1;
                 } else if (roll_angle > 45) {
            	
                	 fix_azimuth=azimuth_angle +90.0f;
                 	if(fix_azimuth > 360.0f){
                 		fix_azimuth = fix_azimuth- 360.0f;;
                 	}
                 	cr.orient=2;
                 	//"Right side of the phone is Up!");
				
                 } else if (roll_angle < -45) {

                	 fix_azimuth=azimuth_angle -90.0f;
                 	if(fix_azimuth < 0){
                 		fix_azimuth = 360.0f +fix_azimuth;
                 	}
                 	cr.orient=3;
                 	
            	        //"Left side of the phone is Up!");
                 }
		Log.v("Show: ", "P: "+ pitch_angle + "-P: "+ (-pitch_angle) + " R: "+roll_angle+ " A: "+azimuth_angle + " FA: "+fix_azimuth);
		
			if(!change && pitch_angle <= 20.0 && pitch_angle >= -20.0  && roll_angle >=-30 && roll_angle <= 30) {
				//Log.v("Changed: ", "with value: "+pitch_angle);
				change=true;
				Logger.log(new FlatActivityLoggerEvent(LoggerEvent.VIEWCHANGE_EVENT));
				finish();
			}
			else {
				cr.mRotZ  = azimuth_angle;
				cr.mRotX = pitch_angle;
				cr.mRotY = roll_angle;
				cr.fix_mRotZ = fix_azimuth;
				mGLSurfaceView.requestRender();
			}

	}
	
	
	//speak the user text
	protected void speakWords(final String speech) {

		//speak straight away
		 class Speak implements Runnable {
             
             @Override
             public void run() {
            	 myTTS.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
             }

             int i;
         }

         Thread t = new Thread(new Speak());

         t.start();
	}

	protected boolean isSpeaking(){
		if(myTTS!=null)
			return myTTS.isSpeaking();
		else
			return false;
	}
	
	protected void stopSpeaking(){
		myTTS.stop();
		//myTTS.shutdown();
	}
	
	//act on result of TTS data check
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

	if (requestCode == MY_DATA_CHECK_CODE) {
		if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
			//the user has the necessary data - create the TTS
		myTTS = new TextToSpeech(this, this);
		}
		else {
				//no data - install it now
			Intent installTTSIntent = new Intent();
			installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
			startActivity(installTTSIntent);
			}
		}
	}

	//setup TTS
	public void onInit(int initStatus) {

		//check for successful instantiation
	if (initStatus == TextToSpeech.SUCCESS) {
		if(myTTS.isLanguageAvailable(Locale.US)==TextToSpeech.LANG_AVAILABLE)
			myTTS.setLanguage(Locale.US);
	}
	else if (initStatus == TextToSpeech.ERROR) {
		Toast.makeText(this, "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
		}
	}

	
	}

class FlatActivityLoggerEvent extends LoggerEvent {
	
	/**
	 * 
	 * @param activity
	 * @param eventType  Should be one of those found in LoggerEvent class.
	 */
	public FlatActivityLoggerEvent(int eventType) {
		super(eventType);
	}
	
	@Override
	public String makeReport() {
		String report = formatDate();
		report += " ";
		report += EVENT_TYPES[getEventType()];
		report += ": Camera activity ";
	
		//Doesn't work like this
//		Configuration config = activity.getResources().getConfiguration();
//		if(config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//			report += "(vertical)";
//		} else if(config.orientation == Configuration.ORIENTATION_PORTRAIT) {
//			report += "(horizontal)";
//		} else {
//			report += "(unkown orientation)";
//		}
		
		return report;
	}
	
}
