package com.example.geogame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

/*
 * To use: create a new instance and use read() to read files
 * Note: The order of the fields in the XML file matters!
 * 		 It has to be lat, lon, title, text, radius, resourcename.
 *       resourcename can be empty.
 * */
public class XMLInterpreter {
	private Exception storedException;
	private static LinkedList<SpotData> sDataList;
	
//	public static void main(String[] args) {
//	XMLInterpreter n = new XMLInterpreter();
//	n.read("testdata.xml");
//	ArrayList<SpotData> list = n.getSpotDataList();
//
//	for(int i = 0; i < list.size(); i++) {
//		System.out.println(list.get(i).getLatitude());
//		System.out.println(list.get(i).getLongitude());
//		System.out.println(list.get(i).getName());
//		System.out.println(list.get(i).getText());
//	}
//}
	
	public XMLInterpreter() {
		sDataList = null;
		this.storedException = null;
	}
	
	/**
	 * This method attempts to read a file and create SpotData objects from it.
	 * @param filename
	 * @return  true if successful
	 */
	public boolean read(String filename) {
		try {
			
			if(!(new File(filename)).exists()) {
				throw new FileNotFoundException();
			}
			
			sDataList = new LinkedList<SpotData>();
			SAXParserFactory SAXFact = SAXParserFactory.newInstance();
			SAXParser parser = SAXFact.newSAXParser();
			
			DefaultHandler dHandler = new DefaultHandler() {
				
				int i = 0;  //keeps track of the current element on the ArrayList
				int latitudeTemp = 0;
				boolean latitude;
				boolean longitude;
				boolean title;
				boolean text;
				boolean radius;
				boolean resourceName;
				boolean description;
				
				String temporary = "";
			
				
				
				public void startElement(String uri, String localName,
						String element_name, Attributes attributes) throws SAXException {
					if(element_name.equals("latitude")) {
						latitude = true;
					}
					if(element_name.equals("longitude")) {
						longitude = true;
					}
					if(element_name.equals("title")) {
						title = true;
					}
					if(element_name.equals("text")) {
						text = true;
					}
					if(element_name.equals("radius")) {
						radius = true;
					}
					if(element_name.equals("resourceName")) {
						resourceName = true;
					}
					if(element_name.equals("description")) {
						description = true;
					}
				}
				
				public void endElement(String uri, String localName, String element_name) {
					if(element_name.equals("latitude")) {
						latitude = false;
					}
					if(element_name.equals("longitude")) {
						longitude = false;
					}
					if(element_name.equals("title")) {
						title = false;
					}
					if(element_name.equals("text")) {
						text = false;
					}
					if(element_name.equals("radius")) {
						radius = false;
					}
					if(element_name.equals("resourceName")) {
						resourceName = false;
					}
					if(element_name.equals("description")) {
						sDataList.get(i).setDescription(temporary);
						i++;
						description = false;
						temporary = "";
					}
				}
				
				/* Do something if you find XML elements.
				 * This method may be called multiple times for each element!
				 */
				public void characters(char[] ch, int start, int len) throws SAXException {
					String str = new String(ch, start, len);
					if(latitude) {
						sDataList.add(new SpotData());
						latitudeTemp = Integer.parseInt(str);
					}
					if(longitude) {
						sDataList.get(i).setLocation(latitudeTemp, Integer.parseInt(str));
					}
					if(title) {
						sDataList.get(i).setName(str);
					}
					if(text) {
						sDataList.get(i).setText(str);
					}
					if(radius) {
						sDataList.get(i).setRadius(Integer.parseInt(str));
					}
					if(resourceName) {
						sDataList.get(i).setResourceName(str);
					}
					if(description) {
						temporary += str;
					}
				}
			};

			parser.parse(new File(filename), dHandler);

			
		} catch(FileNotFoundException fex) {
			this.storedException = fex;
			return false;
		} catch(SAXException saxex) {
			this.storedException = saxex;
			return false;
		} catch(IOException ioex) {
			this.storedException = ioex;
			return false;
		} catch(Exception e) {
			this.storedException = e;
			return false;
		}

		return true;
	}
	
	public LinkedList<SpotData> getSpotDataList() {
		return sDataList;
	}
	
	public Exception getLastException() {
		return this.storedException;
	}
	

}
