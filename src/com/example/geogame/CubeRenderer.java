package com.example.geogame;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
//import android.provider.CalendarContract.Reminders;
import android.util.FloatMath;
import android.util.Log;
import android.widget.Toast;

class CubeRenderer implements GLSurfaceView.Renderer {
	//static final float SCALING_FACTOR = 1.0f/1000; // experimental value, not accurate but in proper magnitude
	static final float SCALING_FACTOR = 30.0f;
	static final float MAX_DRAWN_DIST = 7.0f; /* If dist is between this and MAX_SPOT_DIST_SHOWN,
	the object is drawn at this distance. Value can't be bigger than this or objects are clipped*/
	static final float MIN_DRAWN_DIST = 3.5f; // this is kinda big, could be higher?
	static final float MAX_SPOT_DIST_SHOWN = 1000.0f; // don't draw objects further away than this.
	static final float radToDeg = (float)(180/Math.PI);
	
	private SpotData spotData;
	
	//private Sensor mSensor;
	//private SensorManager mSensorManager;
	public float mRotX; // orientation sensor values from flat activity
	public float mRotY;
	public float mRotZ; //azimuth
	//private float angle = 0.0f;
	public float fix_mRotZ; // orientation fixed azimuth
	public short orient =0; // orientation 0=top up, 1=bottom up, 2=right side up, 3=left side up 
	
	
	//private float y_mover=0.0f;
	//private float x_mover=0.0f;
	private float dist;
	
	private Context context;
	
    public CubeRenderer(boolean useTranslucentBackground, Context context) {
        mTranslucentBackground = useTranslucentBackground;   
        this.context = context;
        mCube = new Cube();
       }  

    public void onDrawFrame(GL10 gl) {
    	
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);  
        
        //gl.glMatrixMode(GL10.GL_MODELVIEW);     
        gl.glLoadIdentity(); // This just resets the scene with an Id matrix I think
        //gl.glEnableClientState(GL10.GL_VERTEX_ARRAY); 
        //gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
        // Rotate the coordinate system
        gl.glScalef(0.5f, 0.5f, 0.5f); // scale the size to make it visible
        
        // Do thing according to the device orientation
        if(orient == 0 || orient == 1){ // top or bottom side up (orient == 0 || orient == 1)
        	gl.glRotatef(mRotX, 0, 1, 0); //pitch
        	gl.glRotatef(0, 1, 0, 0);
        	
        	gl.glRotatef(mRotZ, 0, 0, 1); // Rotate for angle between viewing angle and east
        }
        
        
        else if(orient==2){ //Left sideup
        	
        	float rotY = -mRotY;
        	
        	if(Math.abs(mRotX)>135){
        		rotY = -90.0f-(90.0f+rotY);
        	}
        	//gl.glRotatef(mRotX, 0, 1, 0);
        	gl.glRotatef(0, 0, 1, 0); //pitch
        	gl.glRotatef(rotY, 1, 0, 0);
        	gl.glRotatef(mRotZ, 0, 0, 1); // Rotate for angle between viewing angle and east
        }
            
        else if(orient==3){ //right sideup
        	//gl.glRotatef(-mRotX, 0, 1, 0); //pitch
        	//gl.glRotatef(-mRotY, 1, 0, 0); //roll
        	float rotY = -mRotY;
        	
        	if(Math.abs(mRotX)>135){
        		rotY = 90.0f+(90.0f-rotY);
        	}
        	Log.d("Corrected YRot", "Old Y: "+ mRotY+" New Y: "+ rotY);
        	//gl.glRotatef(mRotX, 0, 1, 0);
        	gl.glRotatef(0, 0, 1, 0); //pitch
        	gl.glRotatef(rotY, 1, 0, 0);
        	gl.glRotatef(mRotZ, 0, 0, 1); // Rotate for angle between viewing angle and east
        }
        
        for(int i=0; i < GGMapActivity.reminders.size(); i++){
        	spotData = GGMapActivity.reminders.get(i);
        	 calculateMove(spotData);
        	 
        	 
        	 //float azimuth = mRotZ;
        	 float bear = spotData.getBearing();
        	 
        	
        	 
        	 if(bear>90.0f)
        		 bear -= 90.0f;
        	 else
        		bear = 360.0f + bear - 90.0f;
        	 
        	 //if(spotData.inRadius)
        	 // Log.d("bearing check", spotData.getName() +" - "+GGMapActivity.currentLatitude+", "
        	 //+GGMapActivity.currentLongitude+ " : " + " bearing="+spotData.getBearing()+ ", angle="+angle+", corBear="+bear + " azi: "+ mRotZ);
        	  	 
        	
        	 
        	 
            //***
        	 if (spotData.inRadius) {
            	gl.glRotatef(-bear, 0, 0, 1); // Rotate against angle between east and spot
            	//gl.glRotatef(-angle, 0, 0, 1);
             	gl.glTranslatef(0, dist, 0.0f); // Move the scene
             	gl.glRotatef(90.0f, 0, 1, 0); // Flip the text
             	
             	mCube.setText(spotData.getName());
             	mCube.loadGLTexture(gl, this.context);
            	
             	mCube.draw(gl); // Now draw the cube
             	gl.glRotatef(-90.0f, 0, 1, 0);
             	gl.glTranslatef(0, -dist, 0.0f);
             	//gl.glRotatef(angle, 0, 0, 1);
             	gl.glRotatef(bear, 0, 0, 1);
        	 //***
             	}      	
        }
    }  
    
    public void onSurfaceChanged(GL10 gl, int width, int height) {     
    	if(height == 0) { 						//Prevent A Divide By Zero By
			height = 1; 						//Making Height Equal One
		}

		gl.glViewport(0, 0, width, height); 	//Reset The Current Viewport
		gl.glMatrixMode(GL10.GL_PROJECTION); 	//Select The Projection Matrix
		gl.glLoadIdentity(); 					//Reset The Projection Matrix

		//Calculate The Aspect Ratio Of The Window
		GLU.gluPerspective(gl, 45.0f, (float)width / (float)height, 0.1f, 100.0f);

		gl.glMatrixMode(GL10.GL_MODELVIEW); 	//Select The Modelview Matrix
		gl.glLoadIdentity(); 					//Reset The Modelview Matrix
    } 

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {      

    	mCube.loadGLTexture(gl, this.context);
    	
    	gl.glEnable(GL10.GL_TEXTURE_2D);			//Enable Texture Mapping ( NEW )
        gl.glDisable(GL10.GL_DITHER);     
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST);   
        gl.glClearDepthf(1.0f); 					//Depth Buffer Setup
		gl.glEnable(GL10.GL_DEPTH_TEST); 			//Enables Depth Testing
		gl.glDepthFunc(GL10.GL_LEQUAL); 			//The Type Of Depth Testing To Do
        if (mTranslucentBackground) {     
            gl.glClearColor(0,0,0,0);      
            } else {        
                gl.glClearColor(1,1,1,1);     
                }       
        gl.glEnable(GL10.GL_CULL_FACE);     
        gl.glShadeModel(GL10.GL_SMOOTH);     
        gl.glEnable(GL10.GL_DEPTH_TEST);   
        }  

    public void setAngle(float _angle){

    	
    }
    
    private void calculateMove(SpotData spot){
    	
    	float dist2=spot.getDistance();
    	
    	//Counting Angle between locations
    	
    	/*float sLatDif = spot.getLatitude()-GGMapActivity.currentLatitude;
    	float sLongDif = spot.getLongitude()-GGMapActivity.currentLongitude;
    	
    	float relation; */
    	
    	/*dist = FloatMath.sqrt((float)(Math.pow(sLatDif,2) + Math.pow(sLongDif,2)));
    	
    	
    	if (sLongDif >= 0 && sLatDif >= 0) {
    		angle = 270.0f + radToDeg*(float)Math.asin(sLongDif/dist);
    	}
    	else if (sLongDif < 0 && sLatDif >= 0) {
    		angle = 180.0f + radToDeg*(float)Math.asin(sLatDif/dist);
    	}
    	else if (sLongDif < 0 && sLatDif < 0) {
    		angle = 90.0f + radToDeg*(float)Math.asin(-sLongDif/dist);
    	}
    	else if (sLongDif >= 0 && sLatDif < 0){
    		angle =  radToDeg*(float)Math.asin(-sLatDif/dist);
    	}*/
    	 
     	
    	
    	dist = dist2 / SCALING_FACTOR;
    	
    	//dist = dist * SCALING_FACTOR;
    	
    	if (dist > MAX_DRAWN_DIST) {
    		dist = MAX_DRAWN_DIST;	
    	}
    	else if (dist < MIN_DRAWN_DIST) {
    		dist = MIN_DRAWN_DIST;	
    	}
    	
    	//Log.d("locationCheck", spotData.getName() + ": " + "distanceLat="+sLatDif + ", distanceLong="+sLongDif+", distance="+dist+ ", bearing="+spotData.getBearing());
    	
    }
    

    
    
    
    private boolean mTranslucentBackground;  
    private Cube mCube;  

		

}
   