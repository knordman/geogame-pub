package com.example.geogame.logging;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * THIS WARN YOU
 * 
 * Docs after in oldspeak. Untruth, make-ups only. Make-ups make
 * THOUGHTCRIME. Careful. Supervisor rank or not read. This warn you.
 * THOUGHTCRIME in docs after. SEXCRIME in docs after. Careful. If self excited,
 * report. If other excited, report. Everything report. Withhold accurate report
 * is INFOCRIME. This warn you. Are you authorised, if no stop read now! Make
 * report! If fail make report, is INFOCRIME. Make report. If report made on
 * failing to make report, this paradox. Paradox is LOGICRIME. Do not do
 * anything. Do not fail to do anything. This warn you. Why you nervous? Was it
 * you? We know. IMPORTANT: Do not read next sentence. This sentence for
 * official inspect only. Now look. Now don’t. Now look. Now don’t. Careful.
 * Everything not banned compulsory. Everything not compulsory banned. Views
 * expressed within not necessarily those of publisher, editors, writers,
 * characters. You did it. We know. This warn you.
 * 
 * @author Itron
 * 
 */
public abstract class LoggerEvent {

	private Timestamp timestamp;
	private int eventType;
	
	public static final int LOGGING_START_EVENT = -1;
	
//	public static final int NUMBER_OF_EVENTS = 3;
	
	public static final int TAP_EVENT = 0;
	public static final int RESUMED_EVENT = 1;
	public static final int PAUSED_EVENT = 2;
	public static final int PLAYER_EVENT = 3;
	public static final int NEWSPOT_EVENT = 4;
	public static final int VIEWCHANGE_EVENT = 5;
	
	
	public static final String[] EVENT_TYPES = {"TAP", "RESUMED", "PAUSED", "PLAYER", "NEWSPOT", "VIEWCHANGE"};

	
	//Constructors
	public LoggerEvent(int EventType) {
		this.timestamp = new Timestamp(System.currentTimeMillis());
		this.eventType = EventType;
	}
	
	
	//Getters and setters
	
	public int getEventType() {
		return this.eventType;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * This method should be used to create readable date/timestamp.
	 * All log entries should start with this for the sake of consistency.
	 * @return
	 */
	public String formatDate() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		return df.format(this.timestamp).toString();
	}

	/**
	 * An implementation of this method should create a glorious report
	 * that is added to the specified log file by Logger class.
	 * 
	 * A good way to start would be:
	 * String report = formatDate();
	 * report += " ";
	 * report += "EVENT TYPE"; //EVEN TYPE should be replaced with the corresponding
	 * 						   // event.
	 * report += "He's dead Jim."  //From this point on, the custom class will decide
	 * 							   //what to include.
	 * @param  LoggingActivity  May be null.
	 * @return  The report as a string
	 */
	public abstract String makeReport();
}
