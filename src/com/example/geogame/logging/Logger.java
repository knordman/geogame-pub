package com.example.geogame.logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

public class Logger {
	//Change to false to start logging - stopped for now to avoid huge log file to be created
	private static final boolean STOP_LOGGING = true;
	
	public static boolean error = false;
	private static int errorcode = -1;
	
	/*
	 * These integers represent errorcodes.
	 */
	public static final int FAILED_TO_CREATE_FILE = 0;
	public static final int FAILED_TO_WRITE_TO_FILE = 1;
	
	/*
	 * Some variables we want to keep safe.
	 */
	private static final String logfileName = "sdcard/GeoGameData/geogame.log";  //This shall be the default name
	private static File logfile;
	private static ArrayList<LoggerEvent> lastEvent = new ArrayList<LoggerEvent>();
	
	/*
	 * counters:
	 * actually event specific things
	 */
	private static int taps = 0;
	
	static {
		logfile = new File(logfileName);
		if(!logfile.exists()) {
			try {
				logfile.createNewFile();
			} catch(IOException ioex) {
				error = true;
			
				errorcode = FAILED_TO_CREATE_FILE;
			}
		}
		
		//Add a ghost event for each type of event
		for(int i = 0; i < LoggerEvent.EVENT_TYPES.length; i++) {
			lastEvent.add(null);
		}
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(logfile, true));
			writer.append(new LoggerLoggerEvent().makeReport());
			writer.newLine();
			writer.flush();
			writer.close();
		} catch(IOException ioex) {
//			error = true;
			errorcode = FAILED_TO_WRITE_TO_FILE;
		}
	}
	
	/**
	 * This method calls the event's makeReport and writes the return value
	 * into the log file.
	 * @param event  Event to be logged.
	 */
	public static void log(LoggerEvent event) {
		if(!error && !STOP_LOGGING) {
			try {
				BufferedWriter writer = new BufferedWriter(new FileWriter(logfile, true));
				
				writer.append(event.makeReport());
				writer.append(" ");
				
				if(event.getEventType() == LoggerEvent.TAP_EVENT) {
					taps++;
					writer.append("(" + taps + " so far) ");
				}
				
				writer.append(timePassedSinceLastEvent(event));
				
				writer.newLine();
				writer.flush();
				writer.close();
				
				lastEvent.set(event.getEventType(), event);
			} catch(IOException ioex) {
//				error = true;
				errorcode = FAILED_TO_WRITE_TO_FILE;
			}
		}
	}
	
	
	/*
	 * Creates a string explaining how much time has passed since the last event of this type.
	 */
	private static String timePassedSinceLastEvent(LoggerEvent event) {
		if(lastEvent.get(event.getEventType()) == null) {
			return "";
		}
		
		return "" + timePassed(lastEvent.get(event.getEventType()).getTimestamp(), event.getTimestamp()) + " has passed since last event of this type";
	}
	
	/*
	 * Compare the two timestamps and return a nifty string explaining how much time passed between the two timestamps.
	 */
	private static String timePassed(Timestamp one, Timestamp two) {
		long seconds = 0;
		long minutes = 0;
		long hours = 0;
		long time = Math.abs(two.getTime() - one.getTime());
		time = time/1000;
		seconds = time % 60;
		minutes = (time / 60) % 60;
		hours = (time / 3600);
		
		return "" + hours + "h " + minutes + "m " + seconds + "s";
	}
	
	public static int getErrorcode() {
		return errorcode;
	}
}

/*
 * Only used once, when the program starts.
 */
class LoggerLoggerEvent extends LoggerEvent {

	public LoggerLoggerEvent() {
		super(LOGGING_START_EVENT);
	}
	
	@Override
	public String makeReport() {
		String report = "";
		report += "\n"
				+ "Starting new log at "
				+ formatDate();
		return report;
	}
	
}
