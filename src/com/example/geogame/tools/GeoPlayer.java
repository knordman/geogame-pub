
package com.example.geogame.tools;

import java.lang.reflect.Field;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import com.example.geogame.R;
import com.example.geogame.logging.Logger;
import com.example.geogame.logging.LoggerEvent;

/**
 * Geoplayer uses the android.media.MediaPlayer to play resource music files.
 * These files are stored in the res/raw folder. The files should be 100% valid
 * or the player may not work as intended or at all.
 * 
 * A player requires a context that it is bound to and whatever that context is,
 * it should remember to call destroy() for the player when the context is paused
 * or destroyed.
 * 
 * Once created using the player is as easy as calling toggle(String resource)
 * where 'resource' is the name of the resource. If the player is already playing
 * a sound file, another call to toggle() will cause the player to stop and release
 * all resources. toggle() can only be invoked every half a second, more frequent
 * invokations will be ignored.
 * @author Itron
 *
 */
public class GeoPlayer {
	private Context context;
	private MediaPlayer mp;
	
	/**
	 * Create a new GeoPlayer
	 * @param context  The context responsible for this player.
	 */
	public GeoPlayer(Context context) {
		this.context = context;
	}
	
	/**
	 * Toggles the player between play and stop.
	 * @param resource  The name of the sound file
	 * @return  'true' when was successful and 'false' when not
	 */
	public boolean toggle(String resource) {
		if(mp != null && mp.isPlaying()) {
			mp.reset();
			mp.release();
			mp = null;
			Log.d("Playing", "File: " + resource);
			Logger.log(new GeoPlayerLoggerEvent(LoggerEvent.PLAYER_EVENT, null));
			return true;
		}
		if(mp != null) {
			mp.reset();
			mp.release();
			mp = null;
		}
		
		
		
		Field field;
		try {
			field = R.raw.class.getField(resource);
			mp = MediaPlayer.create(context, field.getInt(null));
		} catch (NoSuchFieldException fex) {
			return false;
		} catch (IllegalAccessException iex) {
			return false;
		}

		try {
			mp.start();
			Logger.log(new GeoPlayerLoggerEvent(LoggerEvent.PLAYER_EVENT, resource));
		} catch(Exception e) {
			return false;
		}
		
		return true;
		
	}
	
	/**
	 * Should always be called once you are done with the player
	 * to release resources.
	 */
	public void destroy() {
		if(mp != null) {
			mp.reset();
			mp.release();
			mp = null;
		}
	}
	
	
	public boolean isPlaying(){
		
		if(mp!=null)
			return mp.isPlaying();
		else 
			return false;
	}
	
}

class GeoPlayerLoggerEvent extends LoggerEvent {
	private String resourceName;

	/**
	 * 
	 * @param eventType  Should be one of those found in LoggerEvent class.
	 * @param resourceName  null means: stopped playing. Anything else is the name of the 
	 *                      sound resource.
	 */
	public GeoPlayerLoggerEvent(int eventType, String resourceName) {
		super(eventType);
		this.resourceName = resourceName;
	}
	
	public String makeReport() {
		String report = formatDate();
		report += " ";
		report += EVENT_TYPES[getEventType()];
		if(resourceName == null) {
			report += ": Player has stopped playing";
		} else {
			report += ": Player now playing -- " + resourceName + " --";
		}
		
		return report;
	}
 }
