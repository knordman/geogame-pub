package com.example.geogame;



/*
 * Copyright (C) 2013 GeoGame Project, University of Helsinki
 * 
 * Uses code from from The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.widget.Toast;

import com.example.geogame.logging.Logger;
import com.example.geogame.logging.LoggerEvent;
import com.example.geogame.tools.GeoPlayer;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;



public class GGMapActivity extends MapActivity implements SensorEventListener, OnInitListener{
	private LocationManager locationManager = null;
	static LinkedList<SpotData> reminders = new LinkedList<SpotData>();		// 
	//static LinkedList<ReminderData> storedLocations = new LinkedList<ReminderData>();	// List of all stored locations
	
	private SensorManager mSensorManager;
	private Sensor mOrientation;
	private MapView mapView;
	private Vibrator vibra;
	private int n_of_overlays;
	
	//private Sensor accelometer;
	
	private boolean centered;
	private boolean close = false;;
	
	// Minimum time between vibrations from same spot
	private final int vibraTreshold = 4000;
	
	
	static boolean cameraOn;
	
	static int newSpots =1;
	
	 static int currentLatitude, currentLongitude;
	 static Location currentLocation;
	 //static float currentBearing = -1000;
	 //static float currentDistance;
	// static int closeSpotIdx = -1; // If -1, no close by spots
	 //static int[] closeSpots;
	 public static final int NEW_SPOT=0;
	 
	 
	 int dot = 200;      // Length of a Morse Code "dot" in milliseconds
	 int dash = 500;     // Length of a Morse Code "dash" in milliseconds
	 int short_gap = 200;    // Length of Gap Between dots/dashes
	 int medium_gap = 500;   // Length of Gap Between Letters
	 int long_gap = 1000;    // Length of Gap Between Words
	 long[] pattern = {
	     0,  // Start immediately
	     dash, short_gap, dot, short_gap,
	     dash, short_gap, dot, short_gap, 2*dash,
	     long_gap+medium_gap
	     //, dot, short_gap, dot, short_gap, dot,    // s
	     //long_gap
	 };

	 long[] pattern2 = {
		     0,  // Start immediately
		     dot, short_gap, dot,     // s
		     short_gap,
		     dash,  // o
		     long_gap+medium_gap
		 
		 };
	 


	 GeoPlayer player;
	 
	 
	//TTS object
 	static TextToSpeech myTTS;
	//status check code
 	private int MY_DATA_CHECK_CODE = 1;
	 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		
		
		GGApp appData = ((GGApp)getApplicationContext());
	    appData.player = new GeoPlayer(this);
	    player = appData.player;
	    
	   // myTTS = appData.myTTS;

		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		 mapView.setSatellite(false);
		MapController mapController = mapView.getController();
		mapController.setZoom(17);  
		
		
		// Get instance of Vibrator from current Context
		vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

		// MapController mapController = mapView.getController();
		
	
	

		locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		LocationProvider provider =
				locationManager.getProvider(LocationManager.GPS_PROVIDER);

		// Register the listener with the Location Manager to receive location updates
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);	

		// Retrieve a list of location providers that have fine accuracy, no monetary cost, etc
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setCostAllowed(false);
		String providerName = locationManager.getBestProvider(criteria, true);

		// If no suitable provider is found, null is returned.
		if (providerName != null) {


			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
					10000,          // 10-second interval.
					10,             // 10 meters.
					locationListener);
		}
		
		//Sensor stuff
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		//accelometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		
		
		
		//check for TTS data
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);
		
        
        
		// Reading spots from xml file
		XMLInterpreter n = new XMLInterpreter();
		n.read("sdcard/GeoGameData/testdata.xml");
		reminders = n.getSpotDataList();
		//hardCodeSpots();
		if(reminders==null){
			reminders=new LinkedList<SpotData>();
			hardCodeSpots();
			//
			
		}
		
		showOverlays();
		
		
		
		

		

	}
	
	private void hardCodeSpots(){
		//SpotData kotikoe = new SpotData("Wallininkatu 10", "Koti", 60185086, 24944394, 250, "");
		/*kotikoe.setDescription("Havis Amanda was sculpted by Ville Vallgren. It was unveiled on September 20, 1908. " +
				"The work drew a lot of criticism at first, especially from women. " +
				"Its nakedness and seductiveness were considered inappropriate. The sea lions, " +
				"with their human tongues hanging out, were said to represent men lusting after the mademoiselle.");
		*/
		//reminders.add(kotikoe);
		
		/*reminders.add(new SpotData("Wallininkatu 10", "Koti", 60185374, 24943492, 250, "kumpula"));
		
		SpotData koti = new SpotData("Wallininkatu 10", "Kotini on kullan kallis paikka, jossa voin levätä", 60185086, 24944394, 250, "");
		
		koti.setDescription("The Three Smiths Statue is a sculpture by Felix Nylund.This realistic statue, unveiled in 1932, " +
				"depicts three smiths hammering on an anvil.The statue was damaged in a bombing during the Continuation War in 1944." +
				" Marks of the damage can still be seen in the base of the statue, and the anvil has a hole caused by a bomb shrapnel.");	
		
		reminders.add(koti);*/
		
		
		reminders.add(new SpotData("University of Helsinki", "Kumpula Campus", 60204624, 24962035, 200, "kumpula"));
		
		reminders.add(new SpotData("Designed by famous Finnish arcitect Eliel Saarinen", "Railway station", 60170982, 24941336, 250, "railway"));
		
		
		reminders.add(new SpotData("Beautiful white cathedral, that was built in 1830-1852. \nIt was known as St. Nicholas' Church until 1917. ","Helsinki Cathedral", 60170471, 24952081, 250, "cathedral"));
		
		
		SpotData threeSmiths = new SpotData("Statue of three smiths sculptured by Felix Nylund, unveiled in 1932. \n" +
				"Marks of World War II bombing can still be seen on the base of the statue.","Three Smiths", 60168665, 24940955, 150, "three_smiths");
		threeSmiths.setDescription("The Three Smiths Statue is a sculpture by Felix Nylund.This realistic statue, unveiled in 1932, " +
				"depicts three smiths hammering on an anvil.The statue was damaged in a bombing during the Continuation War in 1944." +
				" Marks of the damage can still be seen in the base of the statue, and the anvil has a hole caused by a bomb shrapnel.");		
		reminders.add(threeSmiths);
		
	
		SpotData havis = new SpotData("This statue sculptured by Ville Vallgren was unveiled in 1908.\n " +
				"It presents nude mermaid figure standing in seaweed surrounded by fish and sea lions.","Havis Amanda", 60167557, 24951448, 150, "");
		havis.setDescription("Havis Amanda was sculpted by Ville Vallgren. It was unveiled on September 20, 1908. " +
				"The work drew a lot of criticism at first, especially from women. " +
				"Its nakedness and seductiveness were considered inappropriate. The sea lions, " +
				"with their human tongues hanging out, were said to represent men lusting after the mademoiselle.");
		reminders.add(havis);
		
		
		SpotData stock = new SpotData("Stockmann department store. \nBiggest department store in Nordic countries.","Stockmann Helsinki",60168364, 24942438, 250, "");
		stock.setDescription("Stockmann was established by Georg Franz Stockmann, " +
				"a German merchant from Lübeck. In 1859 Stockmann became the manager of a store in Helsinki's Senate Square. " +
				"In 1862, G.F. Stockmann took control of the store and Stockmann was officially established. " +
				"In 1902 the company became G.F. Stockmann Aktiebolag. The shareholders were Stockmann and his two sons, " +
				"Karl and Frans. In 1930 the building which houses the current headquarters of the department store was finished, " +
				"complete with revolving doors, a soda fountain and escalators. Nowadays it is a biggest department store in nordic countries.");
		reminders.add(stock);
		
		
		SpotData esplanadi = new SpotData("Park in the middle of Helsinki.\nPopular picnic spot during the summer.\nLive music and bronze statues.","Esplanadi Park",60167487, 24947591, 250, "");
		esplanadi.setDescription("This is Esplanadi Park. The big green area between the two streets is very active in summer, " +
				"where many Finns come to have a picnic. There are also numerous live music performances on a special outdoor stage in front of Cafe Kappeli. " +
				"There is a statue of Johan Ludwig Runeberg by his son Walter Runeberg in the park. The park was originally opened in 1812.");
		reminders.add(esplanadi);
		
	}

	protected void showOverlays(){
		
		List<Overlay> mapOverlays = mapView.getOverlays();    
		
		Drawable drawable = this.getResources().getDrawable(R.drawable.red_cross);
		MapMarkerOverlay itemizedoverlay = new MapMarkerOverlay(drawable, this, mapView, player);
		
		Drawable drawable_green = this.getResources().getDrawable(R.drawable.green_cross);
		MapMarkerOverlay radiusoverlay = new MapMarkerOverlay(drawable_green, this, mapView, player);
		// Create markers for the reminders.
       
		for (int i = n_of_overlays; i< reminders.size(); i++){
        	SpotData temp= reminders.get(i);
			if(temp.inRadius)
				radiusoverlay.addMarker(temp, i);
			else
				itemizedoverlay.addMarker(temp, i);
        	
        }
        n_of_overlays = reminders.size();
        
		

		mapOverlays.add(itemizedoverlay);
		mapOverlays.add(radiusoverlay);

		// Create the overlay for displaying the user's location.
		UserLocationOverlay locationOverlay = new UserLocationOverlay(this, mapView);
		locationOverlay.enableCompass();
		mapOverlays.add(locationOverlay);
        
        
	}
	
	OverlayItem createDot (int lat, int lon, String title, String message) {
		return new OverlayItem(new GeoPoint(lat,lon), title, message);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_map, menu);
		return true;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void onStart() {
		super.onStart();

		// This verification should be done during onStart() because the system calls
		// this method when the user returns to the activity, which ensures the desired
		// location provider is enabled each time the activity resumes from the stopped state.
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

		if (!gpsEnabled) {
			// Build an alert dialog here that requests that the user enable
			// the location services, then when the user clicks the "OK" button,
			// call enableLocationSettings()


		}
	}
	@Override
	protected void onStop() {
		super.onStop();
		//locationManager.removeUpdates(locationListener);
		mSensorManager.unregisterListener(this);
	
	}

	// Restores UI states after rotation.
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(centered)
			centerMap();
		
		 
		
		mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_NORMAL);
		Log.d("GGMap", "after center");
		Logger.log(new GGMapActivityLoggerEvent(LoggerEvent.RESUMED_EVENT, null));
	}
	
	@Override
	protected void onPause() {
		super.onPause();
        mSensorManager.unregisterListener(this);
       
        
        Logger.log(new GGMapActivityLoggerEvent(LoggerEvent.PAUSED_EVENT, null));
	}

	@Override
	protected void onDestroy(){
		if(myTTS != null) {

	    	//myTTS.stop();
	    	myTTS.shutdown();
	        Log.d("myTTS", "TTS Destroyed");
	    }
	    super.onDestroy();
		
	}
	
	private void enableLocationSettings() {
		Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		startActivity(settingsIntent);
	}

	/* If these overrides below give you an error "must override superclass", change your code compliance level to Java 1.6 in Eclipse:
	 * Go to Project -> Properties -> Java Compiler. There make sure that Compiler compliance level is set to 1.6.
	 *  You should set that level as a default value by navigating to Window -> Preferences -> Java -> Compiler. 
	 *  Select 1.6 for Compiler compliance level.
	 */

	// Define a listener that responds to location updates
	private final LocationListener locationListener = new LocationListener() {

		@Override
		public void onLocationChanged(Location location) {
			// A new location update is received.  Do something useful with it.
			float distance;
			//float bear;
			currentLocation = location;
        	currentLatitude = (int)(location.getLatitude() * 1e6);
        	currentLongitude = (int)(location.getLongitude() * 1e6);
        	Location tempLoc; SpotData tempData;
        	
        	long now = System.currentTimeMillis();
        	
        	//Toast.makeText(getApplicationContext(), "Update location", Toast.LENGTH_SHORT).show();
        	
        	 // Move the controller to our current location, or previous coordinates if provided.
            if(!centered){
            	centerMap();
            }
          
            
            int close_count=0;
            
            List<Overlay> mapOverlays = mapView.getOverlays();  
			for (int i = 0; i < reminders.size(); i++){
				
				tempData= reminders.get(i);
				tempLoc = tempData.getLocation();
				distance = location.distanceTo(tempLoc);
				tempData.setDistance(distance);
				
				tempData.setBearing(location.bearingTo(tempLoc));
				
						
				if (distance > 0 && distance < tempData.getRadius() ) { // Spot in range
					
					// is spot in given radius
					
					close_count++;
					if(!tempData.inRadius){ 
						tempData.inRadius=true;
						// Only perform this pattern one time (-1 means "do not repeat")
						
						if(tempData.radiusVibraTime==0 || (now - tempData.radiusVibraTime) >vibraTreshold  ){
								tempData.vibraIn=true;
								vibra.vibrate(pattern, -1);
								tempData.radiusVibraTime=now;
								Toast.makeText(getApplicationContext(), "\""+tempData.getName()+ "\" is in range" , Toast.LENGTH_LONG).show();
								//Toast.makeText(getApplicationContext(), tempData.getName()+ " is in radius | Distance: "+ Math.round(tempData.getDistance())+" m" , Toast.LENGTH_LONG).show();
							}
						
						
						if(!mapOverlays.isEmpty()) 
					     { 
					     mapOverlays.clear(); 
					     n_of_overlays =0;
					     mapView.invalidate();
					     showOverlays(); 

					     }

					}
					else if(!tempData.vibraIn && (tempData.radiusVibraTime==0 || (now - tempData.radiusVibraTime) >vibraTreshold  ) ){
						 tempData.vibraIn=true;
						 vibra.vibrate(pattern, -1);
						tempData.radiusVibraTime=now;
						//Toast.makeText(getApplicationContext(), tempData.getName()+ " is in radius | Distance: "+ Math.round(tempData.getDistance())+" m" , Toast.LENGTH_LONG).show();
						Toast.makeText(getApplicationContext(), "\""+tempData.getName()+ "\" is in range" , Toast.LENGTH_LONG).show();
						 
					}
					
					else // Spot not in range
					if(!close)
						close=true;
					
					
					
		
					
					//Toast.makeText(getApplicationContext(), "Close, "+ reminders.get(i).getText()+ ", " + bear+", "+distance, Toast.LENGTH_LONG).show();

					//Toast.makeText(getApplicationContext(), "lat dif: "+ (reminders.get(i).getLatitude()-currentLatitude) + " lon dif: " + (reminders.get(i).getLongitude()-currentLongitude) +
					//										" dist: "+tempData.getDistance(), Toast.LENGTH_LONG).show();
		
				}
				else{
					if(tempData.inRadius){
						
						
						if(tempData.radiusVibraTime==0 || (now - tempData.radiusVibraTime) >vibraTreshold  ){
						  tempData.vibraIn=false;
						  vibra.vibrate(pattern2, -1);
						  tempData.radiusVibraTime=now;
						  Toast.makeText(getApplicationContext(), "\""+tempData.getName()+ "\" is OUT of range" , Toast.LENGTH_LONG).show();
						  
						}
						tempData.inRadius=false;
						if(!mapOverlays.isEmpty()) { 
					    mapOverlays.clear(); 
					     n_of_overlays =0;
					     //mapView.invalidate();
					     showOverlays();

					     }
					     
					}
					else{
						if(tempData.vibraIn && (tempData.radiusVibraTime==0 || (now - tempData.radiusVibraTime) >vibraTreshold )){
							tempData.vibraIn=false;
							vibra.vibrate(pattern2, -1);
							tempData.radiusVibraTime=now;
							 Toast.makeText(getApplicationContext(), "\""+tempData.getName()+ "\" is OUT of range" , Toast.LENGTH_LONG).show();
							
						}
						
						tempData.inRadius=false;
					}
				}	
			}
		
			if(close_count==0 && close){
				//vibra.vibrate(pattern2, -1);
				
			/*	if(!mapOverlays.isEmpty()) 
			     { 
			    mapOverlays.clear(); 
			     n_of_overlays =0;
			     //mapView.invalidate();
			     showOverlays();

			     }*/
				
				close = false;
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};
	
	private void centerMap(){
		MapController mapController = mapView.getController();
    	GeoPoint startlocation = new GeoPoint(currentLatitude, currentLongitude);
    	mapController.animateTo(startlocation);
    	centered = true;
	}

	//Spot can be saved to the file and restored from file with these methods. Not in use at the moment
	private void saveState(Context ctx) {
		// Saves reminders and stored locations.
		ObjectOutputStream oos;
		String reminderfilename = "Reminders";
		//String locationfilename = "Locations";

		FileOutputStream fos;
		try {
			fos = ctx.openFileOutput(reminderfilename, Context.MODE_PRIVATE);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(reminders); 
			oos.close();
			/*fos = ctx.openFileOutput(locationfilename, Context.MODE_PRIVATE);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(reminders); 
			oos.close();*/
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	// Method for reading SpotaData from the file
	private static void restoreState(Context ctx) {
		// Restores reminders and stored locations
		ObjectInputStream ois;
		String reminderfilename = "Reminders";
		//String locationfilename = "Locations";

		FileInputStream fos;
		try {
			fos = ctx.openFileInput(reminderfilename);
			ois = new ObjectInputStream(fos);
			reminders = (LinkedList<SpotData>) ois.readObject(); 
			ois.close();

			/*fos = ctx.openFileInput(locationfilename);
			ois = new ObjectInputStream(fos);
			reminders = (LinkedList<SpotData>) ois.readObject(); 
			ois.close();*/

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	/** Determines whether one Location reading is better than the current Location fix.
	 * Code taken from
	 * http://developer.android.com/guide/topics/location/obtaining-user-location.html
	 *
	 * @param newLocation  The new Location that you want to evaluate
	 * @param currentBestLocation  The current Location fix, to which you want to compare the new
	 *        one
	 * @return The better Location object based on recency and accuracy.
	 */
	protected Location getBetterLocation(Location newLocation, Location currentBestLocation) {
		final int TWO_MINUTES = 1000 * 60 * 2;

		if (currentBestLocation == null) {
			// A new location is always better than no location
			return newLocation;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = newLocation.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use the new location
		// because the user has likely moved.
		if (isSignificantlyNewer) {
			return newLocation;
			// If the new location is more than two minutes older, it must be worse
		} else if (isSignificantlyOlder) {
			return currentBestLocation;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (newLocation.getAccuracy() - currentBestLocation.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(newLocation.getProvider(),
				currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and accuracy
		if (isMoreAccurate) {
			return newLocation;
		} else if (isNewer && !isLessAccurate) {
			return newLocation;
		} else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
			return newLocation;
		}
		return currentBestLocation;
	}
	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}

	// AsyncTask encapsulating the reverse-geocoding API.  Since the geocoder API is blocked,
	// we do not want to invoke it from the UI thread.
	private class ReverseGeocodingTask extends AsyncTask<Location, Void, Void> {
		Context mContext;

		public ReverseGeocodingTask(Context context) {
			super();
			mContext = context;
		}

		@Override
		protected Void doInBackground(Location... params) {
			Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

			Location loc = params[0];
			List<Address> addresses = null;
			try {
				addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
			} catch (IOException e) {
				e.printStackTrace();
				// Update address field with the exception.
				//Message.obtain(mHandler, UPDATE_ADDRESS, e.toString()).sendToTarget();
			}
			/*
			 * Esimerkkikoodi, jossa otetaan listan ensimmÃ¤inen osoite ja nÃ¤ytetÃ¤Ã¤n ruudulla.
	            if (addresses != null && addresses.size() > 0) {
	                Address address = addresses.get(0);
	                // Format the first line of address (if available), city, and country name.
	                String addressText = String.format("%s, %s, %s",
	                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
	                        address.getLocality(),
	                        address.getCountryName());
	                // Update address field on UI.
	                Message.obtain(mHandler, UPDATE_ADDRESS, addressText).sendToTarget();
	            }*/
			return null;
		}
	}

	// Finds a list of addresses (including coordinates) for given location name

	private void doGeocoding(String location) {
		// Since the geocoding API is synchronous and may take a while.  You don't want to lock
		// up the UI thread.  Invoking geocoding in an AsyncTask.
	}
	private class GeocodingTask extends AsyncTask<String, Void, Void> {
		Context mContext;

		public GeocodingTask(Context context) {
			super();
			mContext = context;
		}

		@Override
		protected Void doInBackground(String... params) {
			Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

			String loc = params[0];
			List<Address> addresses = null;
			try {
				addresses = geocoder.getFromLocationName(loc, 1);
				Address address = addresses.get(0);
				if(address.hasLatitude() && address.hasLongitude()){
					double selectedLat = address.getLatitude();
					double selectedLng = address.getLongitude();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		float azimuth_angle = event.values[0]; //0 to 359
		float pitch_angle = event.values[1]; //180 to -180
		float roll_angle = event.values[2]; //90 to -90

		//Toast.makeText(this, "P: "+ pitch_angle + " R: "+roll_angle+ " A: "+azimuth_angle, Toast.LENGTH_LONG).show();
		//Log.v("Show: ", "P: "+ pitch_angle + " R: "+roll_angle+ " A: "+azimuth_angle);
		
	
			if((pitch_angle >= -115.0 && pitch_angle <= -65.0 && roll_angle >=-30 && roll_angle <= 30) || 
					((roll_angle <= -70 || roll_angle >= 70)  && ((pitch_angle>=-10 && pitch_angle <= 10) || pitch_angle<=-165)) )  {
				//Log.v("Changed: ", "with value: "+pitch_angle);
				Logger.log(new GGMapActivityLoggerEvent(LoggerEvent.VIEWCHANGE_EVENT, null));
				Intent intent = new Intent(this, FlatActivity.class);
				//Intent intent = new Intent(this, CameraActivity.class);
				startActivity(intent);
			}

	}
	
	public static int getOrient(Activity act){
	    Display getOrient = act.getWindowManager().getDefaultDisplay();
	    int orientation = Configuration.ORIENTATION_UNDEFINED;
	    if(getOrient.getWidth()==getOrient.getHeight()){
	        orientation = Configuration.ORIENTATION_SQUARE;
	    } else{ 
	        if(getOrient.getWidth() < getOrient.getHeight()){
	            orientation = Configuration.ORIENTATION_PORTRAIT;
	        }else { 
	             orientation = Configuration.ORIENTATION_LANDSCAPE;
	        }
	    }
	    return orientation;
	}
	
	
	 @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
		 
		 Log.v("ADDING SPOT", "ACT result GGMap");
	        super.onActivityResult(requestCode, resultCode, intent);
	        if (intent == null) {
	        	
	        	return;
	        }
	        Bundle extras = intent.getExtras();
	        if(extras !=null){
	        
	        	 Log.v("ADDING SPOT", "Extras not null");
	        	if(requestCode==NEW_SPOT){
	        		 
	        		int lat = extras.getInt("LAT");
	        		int lon = extras.getInt("LON");
	        		int rad = extras.getInt("RADIUS");
	        		
	        		String name = extras.getString("NAME");
	        		
	        		String text = extras.getString("TEXT");
	        		if(name==null ||name.trim().equals("")){
	        			name = GGMapActivity.newSpots +". New Spot";
	        		
	        		
	        			if(text==null || text.trim().equals(""))
	        				text= "This is new spot "+ GGMapActivity.newSpots;
	        			
	        			GGMapActivity.newSpots++;	
	        		}
	        		Log.v("ADDING SPOT", "Request NEW_SPOT "+lat+ ", "+lon+", "+name+", "+ text);
	        		
	        		SpotData addData = new SpotData(text, name, lat, lon, rad, "");
	        		GGMapActivity.reminders.add(addData);
	        		 Logger.log(new GGMapActivityLoggerEvent(LoggerEvent.NEWSPOT_EVENT, addData));
	        		
	        		showOverlays();
	        		
	        	}
	        	
	        }
	        if (requestCode == MY_DATA_CHECK_CODE) {
				if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
					//the user has the necessary data - create the TTS
				myTTS = new TextToSpeech(this, this);
				}
				else {
						//no data - install it now
					Intent installTTSIntent = new Intent();
					installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
					startActivity(installTTSIntent);
					}
			}
	        
	    }
	 
		//speak the user text
		protected void speakWords(final String speech) {

			 class Speak implements Runnable {
	              
	                @Override
	                public void run() {
	                   
	                  

	                    myTTS.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
	                }

	                int i;
	            }

	            Thread t = new Thread(new Speak());

	            t.start();
			
			
			//speak straight away
	    	
		}
		
		protected boolean isSpeaking(){
			return myTTS.isSpeaking();
		}
		
		protected void stopSpeaking(){
			myTTS.stop();
			//myTTS.shutdown();
		}

		//setup TTS
		public void onInit(int initStatus) {

			//check for successful instantiation
		if (initStatus == TextToSpeech.SUCCESS) {
			if(myTTS.isLanguageAvailable(Locale.US)==TextToSpeech.LANG_AVAILABLE)
				myTTS.setLanguage(Locale.US);
		}
		else if (initStatus == TextToSpeech.ERROR) {
			Toast.makeText(this, "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
			}
		}


}

class GGMapActivityLoggerEvent extends LoggerEvent {
	
	
	private SpotData location;
	
	/**
	 * 
	 * @param eventType  Should be one of those found in LoggerEvent class.
	 */
	public GGMapActivityLoggerEvent(int eventType, SpotData loc) {
		super(eventType);
		location = loc;
	}
	
	
	
	@Override
	public String makeReport() {
		String report = formatDate();
		report += " ";
		report += EVENT_TYPES[getEventType()];
		report += ": Map activity ";
		
		if(location != null) {

			report += ": Use added spot -- ";
			report += "Name was: " + location.getName();
			report += ", Latitude: " + location.getLatitude();
			report += ", Longitude: " + location.getLongitude();
		}
		
		
		return report;
	}
	
}
